This module relies on the entity configuration list code built in http://drupal.org/node/1781372 (Add an API for listing configuration entities). If that patch is not yet committed and not applied, it will not work.

Current version of the patch compatible with this module: http://drupal.org/files/listing-1781372-74.patch
