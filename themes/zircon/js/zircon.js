(function ($, Drupal) {

  "use strict";
  
  Drupal.behaviors.zircon = {
    attach: function (context, settings) {
      $('.btn-btt').smoothScroll({speed: 1000});
      if($("#search-block-form [name='keys']").val() === "") {
        $("#search-block-form input[name='keys']").val(Drupal.t("Keywords"));
      }
      $("#search-block-form input[name='keys']").focus(function() {
        if($(this).val() === Drupal.t("Keywords")) {
          $(this).val("");
        }
      }).blur(function() {
        if($(this).val() === "") {
          $(this).val(Drupal.t("Keywords"));
        }
      });
      $(window).scroll(function() {
        if($(window).scrollTop() > 200) {
            $('.btn-btt').show();
          }
          else {
            $('.btn-btt').hide();
          }
     }).resize(function(){
        if($(window).scrollTop() > 200) {
            $('.btn-btt').show();
          }
          else {
            $('.btn-btt').hide();
          }
      });
      $(".accordion-title").once().bind('click', function(){
        var $content = $(this).parent().next().find(".accordion-content");
        if($(this).hasClass("accordion-close")) {
              $(this).removeClass("accordion-close");
              $(this).addClass("accordion-open");
          } else {
              $(this).removeClass("accordion-open");
              $(this).addClass("accordion-close");
          }
        $content.toggle();
      });
      $(document).ready(function() {
        $(".accordion-content").hide();
      });     
    }
  };
})(jQuery, Drupal);


